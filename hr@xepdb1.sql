select place.region_name,
       place.country_name,
       place.country_id,
       loc.city
from
       (select reg.region_name,
               ct.country_name,
               ct.country_id
       from regions reg
       inner join countries ct
       on reg.region_id = ct.region_id
       where reg.region_name = 'Europe')place
        left outer join locations loc 
        on loc.country_id = place.country_id;
