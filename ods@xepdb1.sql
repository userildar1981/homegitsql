create table clients (id number not null,
                     first_name varchar2(64),
                     last_name varchar2(64),
                      phone number,
                      birth_day date);

create unique index clients_id on clients (id);

alter table clients add constraint clients_pk primary key (id);

create table account_table (id number not null,
                            client_id number not null,
                            account_number number not null,
                            start_day date,
                            end_day date,
                            primary key (id),
                            foreign key (client_id)references clients (id));
                          
                 
create table cards (id number not null,
                    account_id number not null,
                    number_cart number not null,
                    month_end number not null,
                    year_end number not null,
                    primary key (id),
                    foreign key (number_cart) references account_table(id));
create table wirings (date_wiring date,
                      sum_wirings number not null,
                      id number not null,
                      number_cart number not null,
                      foreign key (number_cart) references clients(id));
  insert into clients (id , first_name, last_name, phone, birth_day) values ('1','John','Sullivan','9999999', to_date('2001-09-01','yyyy-mm-dd'));
  
 insert into account_table (id, client_id, account_number, start_day, end_day) values ('1', '001', '0001', to_date('2020-01-01','yyyy-mm-dd'),to_date('2021-01-01','yyyy-dd-mm'));
select * from account_table;
select rowid,r.*
from clients r;
select cl.id,
cl.phone,
at.account_number
from clients cl
inner join account_table at
on at.client_id = cl.id;