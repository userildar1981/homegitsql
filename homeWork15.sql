declare
v_result number;
begin
select 4/0 as res into v_result 
from dual;
dbms_output.put_line('��������� ������� ����� ' ||v_result);
exception
when zero_devide then
dbms_output.put_line('������!');
end;

declare 
v_result number;
out_of_range exception;
pragma exception_init(raise_of_range, -20001);
begin
select 2/3 as res 
into v_result
from dual;
if v_result not between 0.9 and 1.3
then raise_application_error(-20001,'out_of_range');
end if;
exception
when out_of_range then
dbms_output.put_line(sqlerrm);
end;

declare
   cursor c_reg
   is 
   select region_name,
         region_id
   from regions;
   
   r_name varchar2(32);
   r_id number;
 begin
        open c_reg;
   loop
        exit when c_reg%notfound;
        fetch c_reg into r_name, r_id;
   dbms_output.put_line(r_name ||' '||r_id);
   end loop;
   close c_reg;
   end;
   
   declare
   cursor c_reg
   is 
   select region_name,
         region_id
   from regions;
   
   
   v_row c_reg%rowtype;
 begin
        open c_reg;
   loop
        exit when c_reg%notfound;
        fetch c_reg into v_row;
   dbms_output.put_line(v_row.region_id ||' '||v_row.region_name);
   end loop;
   close c_reg;
   end;

declare
   cursor c_reg
   is 
   select region_name,
         region_id
   from regions;
   
   
   v_row c_reg%rowtype;
 begin
       for r in c_reg loop
   dbms_output.put_line(r.region_id ||' '||r.region_name);
   end loop;
   end;

declare
   cursor c_reg(p_text varchar2)
   is 
   select region_name,
         region_id
   from regions
   where region_name like '%'||p_text||'%';
   
   
   v_row c_reg%rowtype;
 begin
       for r in c_reg('A') loop
   dbms_output.put_line(r.region_id ||' '||r.region_name);
   end loop;
   end;