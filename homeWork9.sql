create user security
identified by security1
default tablespace sysaux
temporary tablespace temp
account unlock;
alter user security quota unlimited on sysaux;

grant create session to security;

