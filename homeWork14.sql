create or replace package pkg_job_description
as
function get_mean_salary(p_employee_id number) return number;
procedure print_employee_name(id in number,
                              job out varchar2);
procedure get_min_max_salary(job_name in varchar2);
end pkg_job_description;

create or replace  package body pkg_job_description
as
function get_mean_salary(p_employee_id number) return number;
  v_result number;
  begin
  select salary
  into v_result
  from employees
  where employee_id = p_employee_id;
  return v_result;
end get_mean_salary;
procedure print_employee_name(p_id in number,
                              p_job out varchar2)
                              as
employee_name varchar2(32);
v_job varchar2(32);
 begin
 select last_name||' '||first_name 
 into 
 from employees
 where employee_id = p_id;
 
 select job_id
 into v_job
 from employees
 where employee_id = v_job;
 end print_employee_name;
 
procedure get_min_max_salary(job_name in varchar2)
as
salary1 number;
salary2 number;
begin 
select min_salary
into salary1
from jobs
where salary1 = min_salary; 
select max_salary
into salary2
from jobs
where salary2 = max_salary;
end get_min_max_salary;
end;