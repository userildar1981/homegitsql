SELECT TO_CHAR  (sum(salary)) from employees;
SELECT TO_CHAR  (691416, '9G999G999') as salary from dual;

  SELECT emp.*
  FROM employees emp join employees man 
  on emp.employee_id = man.manager_id
 WHERE  emp.hire_date < TO_DATE ('0101', 'DDMM');
 
 SELECT TO_CHAR (hire_date, 'YYYY') year, COUNT (*)
    FROM employees
GROUP BY TO_CHAR (hire_date, 'YYYY');

SELECT TO_NUMBER (TO_CHAR (hire_date, 'YYYY')) year, COUNT (*)
    FROM employees
GROUP BY TO_NUMBER (TO_CHAR (hire_date, 'YYYY'));

SELECT TO_NUMBER (TO_CHAR (hire_date, 'YYYY')) year, COUNT (*), 
    FROM employees
GROUP BY TO_NUMBER (TO_CHAR (hire_date, 'YYYY'));

SELECT MIN (to_char(hire_date, 'YYYY'))
FROM employees;

SELECT MAX (to_char(hire_date, 'YYYY'))
FROM employees;

SELECT AVG (to_char(hire_date, 'YYYY'))
FROM employees;

CREATE TABLE HURRICANE (name varchar2 (64), report_year date, victims number);
COMMENT ON TABLE HURRICANE is 'Справочник ураганов';
ALTER TABLE HURRICANE modify victims not null;
TRUNCATE TABLE HURRICANE;
DROP TABLE HURRICANE;
