create table duty (id varchar2(10), id_employee number (6,0), duty_date date) 

create or replace view security_duty_v
as
select du.id,
       du.duty_date,
       emp.first_name,
       emp.manager_id
from duty du
join inner hr.employees emp
on emp.employee_id = du.id_employee